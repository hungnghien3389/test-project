from mock import patch, MagicMock

from demo import main, check_status_code, get_data, export_to_json


def setup_module(module):
    pass


def test_check_status_code_succeed():
    page = MagicMock()
    page.status_code = 200
    assert check_status_code(page)


def test_check_status_code_failed():
    page = MagicMock()
    page.status_code = 500
    assert check_status_code(page) == False


@patch('bs4.BeautifulSoup')
def test_get_data_succeed(bs_mock):
    page = MagicMock()
    page.content = '<body><p>Hello World</p></body>'
    soup_mock = MagicMock()
    soup_mock.find_all.return_value = ['<p>Hello World</p>']
    bs_mock = MagicMock()
    bs_mock.return_value = soup_mock
    assert get_data(page) == 'Hello World'


@patch('bs4.BeautifulSoup')
def test_get_no_data(bs_mock):
    page = MagicMock()
    page.content = '<body><b>Hello World</b></body>'
    soup_mock = MagicMock()
    soup_mock.find_all.return_value = ['<p>Hello World</p>']
    bs_mock = MagicMock()
    bs_mock.return_value = soup_mock
    assert get_data(page) is None


def test_export_to_json():
    data_mock = "Hello World"
    assert export_to_json(
        data_mock) == '{\n    "data": [\n        {\n            "content": "Hello World"\n        }\n    ]\n}'


@patch('demo.check_status_code')
@patch('demo.get_data')
def test_main(get_data_mock, check_status_mock, capsys):
    check_status_mock.return_value = True
    get_data_mock.return_value = "Hello World"
    main()
    out, err = capsys.readouterr()
    assert err == ""
    assert out == '{\n    "data": [\n        {\n            "content": "Hello World"\n        }\n    ]\n}\n'

