#!/usr/bin/env python

import requests
import json

from bs4 import BeautifulSoup


def check_status_code(page):
    if page.status_code == 200:
        return True
    return False


def get_data(page):
    soup = BeautifulSoup(page.content, 'html.parser')
    list_p = soup.find_all('p')
    if len(list_p) == 0 or list_p is None:
        return None
    return list_p[0].get_text()


def export_to_json(data):
    out = {}
    out['data'] = []
    out['data'].append({
        'content': data
    })
    with open('data.txt', 'w') as outfile:
        json.dump(out, outfile)
    return json.dumps(out, indent=4)


def main():
    url = "http://dataquestio.github.io/web-scraping-pages/simple.html"
    page = requests.get(url)
    if not check_status_code(page):
        print("Page can't download")
        exit(1)

    data = get_data(page)
    if data is None:
        print("No data")
        exit(2)

    print export_to_json(data)


if __name__ == '__main__':
    main()
